<?php

namespace App\Modules\Payment;

use App\Modules\AjaxInterface;
use App\Modules\AjaxResult;
use Illuminate\Http\Request;

class PaymentAjax implements AjaxInterface
{
    use AjaxResult;

    /**
     * @var
     */
    private $params;

    /**
     * @param Request $request
     *
     * @return $this|AjaxInterface
     */
    public function setParams(Request $request) : AjaxInterface
    {
        $params = $request->all();

        $this->params = [
            'order_id' => $params['id'],
            'currency' => $params['currency'],
            'version' => 3,
            'action' => 'pay',
            'amount' => $params['price'],
            'description' => '',
            'language' => 'uk',
            'result_url' => $request->getSchemeAndHttpHost() . '/send?status=process',
            'server_url' => $request->getSchemeAndHttpHost() . '/payment/'
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function dispatch() : array
    {
        $liqpay = new LiqPay(env('LIQPAY_PUBLIC_KEY'), env('LIQPAY_PRIVATE_KEY'));

        $form = $liqpay->getForm($this->params);

        return [
            'status' => 'success',
            'msg' => $form
        ];
    }
}
