$(function () {
    let accordeon = $('.accordion__item-title'),
        active = 'accordion__item-title--active';

    accordeon.on('click', function() {
        let isActive = $(this).hasClass(active);

        accordeon.removeClass(active)

        if (!isActive) {
            $(this).addClass(active);
        }
    });
});
