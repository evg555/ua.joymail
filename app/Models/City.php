<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public static function getCityName(int $id): string
    {
        $city = self::where('id', $id)->select('name')->get()->toArray();

        return current($city)['name'];
    }
}
