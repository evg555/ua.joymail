<?php

namespace App\Modules\Order;

use App\Mail\Providers\IndividualOrderProvider;
use App\Modules\AjaxInterface;
use App\Modules\AjaxResult;
use Illuminate\Http\Request;

class IndividualOrderAjax implements AjaxInterface
{
    use AjaxResult;

    /**
     * @var
     */
    private $params;

    /**
     * @param Request $request
     *
     * @return $this|AjaxInterface
     */
    public function setParams(Request $request) : AjaxInterface
    {
        $params = $request->all();
        $this->params = [
            'phone' => $params['phone'] ?? '',
            'message' => $params['message'] ?? '',
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function dispatch() : array
    {
        if (!$this->params['phone']) {
            return $this->sendError('Phone field is empty!');
        }

        try {
            IndividualOrderProvider::handle($this->params);
        } catch (\Exception $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendSuccess('Your order has sent!');
    }
}
