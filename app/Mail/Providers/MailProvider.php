<?php

namespace App\Mail\Providers;

use Illuminate\Support\Facades\Mail;
use App\Mail\CustomerOrder;
use App\Mail\Mailer;
use App\Mail\SupportOrder;

abstract class MailProvider
{
    protected static $subject = 'Joyka';

    /**
     * @return array
     */
    protected static function getEmails()
    {
        preg_match_all('/[a-zA-Z0-9-_]+@[a-z-]+\.[a-z]{2,5}/', env('MAIL_FOR_ORDERS'), $emails);
        return $emails[0] ?: [];
    }

    protected static function send(array $templates, array $data)
    {
        foreach ($templates as $template => $email) {
            if (!$email) {
                continue;
            }

            Mail::to($email)->send(new Mailer($template, static::$subject, $data));
        }
    }

    /**
     * @param array $data
     */
    abstract public static function handle(array $data);
}
