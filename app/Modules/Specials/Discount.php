<?php

namespace App\Modules\Specials;

use App\Modules\AjaxInterface;
use App\Modules\AjaxResult;
use App\Models\Promocode;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

/**
 * Class Discount
 * @package App\Modules\Specials
 */
class Discount implements AjaxInterface
{
    use AjaxResult;

    /**
     * @var
     */
    private $params;

    /**
     * @param Request $request
     *
     * @return $this|AjaxInterface
     */
    public function setParams(Request $request) : AjaxInterface
    {
        $params = $request->all();
        $this->params = [
            'code' => $params['code'] ?: null,
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function dispatch() : array
    {
        foreach ($this->params as $name => $value) {
            if (is_null($value)) {
                return $this->sendError("Param {$name} is empty");
            }
        }

        return $this->activateCode($this->params['code']);
    }

    /**
     * @param string $code
     *
     * @return string[]
     */
    private function activateCode(string $code)
    {
        $promocode = Promocode::where('name', '=', $code)->first();

        $message = static::validateCode($promocode);

        if ($message) {
            return $this->sendError($message);
        }

        $promocode->count_apply--;
        $promocode->save();

        return $this->sendSuccess('Promocode is activated', [
            'type' => $promocode->type,
            'amount' => $promocode->amount,
        ]);
    }

    /**
     * @param Collection $promocode
     *
     * @return string
     */
    private static function validateCode($promocode): string
    {
        $message = '';

        if (empty($promocode)) {
            $message = 'Promocode is not found';
        } else if (!$promocode->active) {
            $message = 'Promocode is not active';
        } else if (Carbon::now() > $promocode->date_expired) {
            $message = 'Promocode is expired';
        } else if ($promocode->count_apply <= 0) {
            $message = 'Promocode has already been applied';
        }

        return $message;
    }
}
