<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const ORDER_STATUS_NEW = 'N';
    const ORDER_STATUS_PAYED = 'P';

    protected $fillable = [
        'message', 'city_id', 'name_sender', 'email_sender', 'tel_sender','name_reciever',
        'tel_reciever', 'promocode','price','currency', 'letter_type'
    ];

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }
}
