<h1>Уважаемый (ая) {{$data['name_sender']}}.</h1>
<hr>
<font size=4px color="#FF0000">
Ваш заказ #{{$data['order_id']}} полностью ОПЛАЧЕН и принят к исполнению.<br>
Общая сумма: {{$data['price']}} {{$data['currency']}}</font>
<hr>
Пожалуйста, не отвечайте на это письмо. Для связи с нами, напишите на ua-dnepr@joymail.biz.
<br>
Спасибо.
<p>С наилучшими пожеланиями, <br> Команда Joymail ))</p>

<hr>
<hr>

<h1>Dear {{$data['name_sender']}}.</h1>
<hr>
<font size=4px color="#FF0000">
Your order #{{$data['order_id']}} has been PAID and accepted for processing.<br>
TOTAL PAID: {{$data['price']}} {{$data['currency']}}</font>
<hr>
Please do not reply for this e-mail. For any questions, please contact us ua-dnepr@joymail.biz
<br>
Thank you.
<p>Best Regards, <br> Team of Joymail LLC</p>

<img src="{{$data['site_url']}}/logo.png">
<hr>
