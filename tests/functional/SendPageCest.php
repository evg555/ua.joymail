<?php

use App\Models\Offer;
use Illuminate\Support\Facades\DB;

class SendPageCest
{
    public function _before(FunctionalTester $I)
    {
        DB::table('orders')->select('message', '=', 'functional test')->delete();
    }

    public function index(FunctionalTester $I)
    {
        $I->amOnPage('/en/send');
        $I->see('CREAT YOUR');
        $I->see('ADDITIONAL ITEMS');
        $I->see('ENTER YOUR DATA');
        $I->see('Pay');
        $I->see('support@joymail.biz');
    }

    public function createOrder(FunctionalTester $I)
    {
        $fields = [
            'letter_type' => 'love-letter',
            'message' => 'functional test',
            'city_id' => '2',
            'name_sender' => 'test',
            'name_reciever' => 'test',
            'email_sender' => 'test@test.ru',
            'tel_sender' => 'test',
            'tel_reciever'=> 'test',
            'price' => '100',
            'currency' => 'UAH',
            'status' => 'N',
            'locale' => 'en'
        ];

        $offers = Offer::select(
            'offers.id',
            'offer_langs.name'
        )->join('offer_langs', 'offer_langs.offer_id', '=', 'offers.id')
            ->where('offer_langs.lang','=', 'en')
            ->get();

        foreach ($offers as $offer) {
            $offerFields['offer-' . $offer->id] = '1';
            $checkedOfferFields[ucfirst($offer->name)] = '1';
        }

        $I->amOnPage('/en/send');
        $I->submitForm('#data-form', array_merge($fields, $offerFields, [
            'promocode' => '',
            'agree' => '1',
            '_token' => csrf_token()
        ]));

        $I->seeResponseCodeIs(200);

        $fields['offers'] = serialize($checkedOfferFields);
        unset($fields['locale']);

        $I->seeInDatabase('orders', $fields);
        $I->seeInDatabase('orders', ['status' => 'N']);
    }
}
