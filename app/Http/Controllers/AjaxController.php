<?php

namespace App\Http\Controllers;

use App\Modules\AjaxResult;
use App\Modules\Order\IndividualOrderAjax;
use App\Modules\Payment\PaymentAjax;
use App\Modules\Specials\Discount;
use Illuminate\Http\Request;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class AjaxController extends Controller
{
    use AjaxResult;

    private $handlers = [
        'discount' => Discount::class,
        'create-form' => PaymentAjax::class,
        'individual' => IndividualOrderAjax::class,
    ];

    /**
     * @param $action
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($action, Request $request)
    {
        $handler = $this->buildHandler($action);

        if (!$handler) {
            return response()->json($this->sendError('Invalid ajax action: ' . $action));
        }

        $result = $handler->setParams($request)->dispatch();

        return response()->json($result);
    }

    /**
     * @param string $action
     *
     * @return false|mixed
     */
    private function buildHandler(string $action)
    {
        if (!array_key_exists($action, $this->handlers)) {
            return false;
        }

        return new $this->handlers[$action];
    }
}

