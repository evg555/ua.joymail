window.$ = window.jQuery = require('jquery');
window.Swal = require('sweetalert2');
require('./bootstrap');
require ('./accordion');
require ('./mobile_menu');
require ('./reviews_slider');
require ('./additional_items');
require('jquery.cookie');

// require ('./libs.min.js');
require ('./calculator.js');
require ('./main.js');
require ('./form.js');
