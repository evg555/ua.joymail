<?php

class MainPageCest
{
    public function _before(FunctionalTester $I)
    {
    }

    public function index(FunctionalTester $I)
    {
        $I->amOnPage('/en');
        $I->see('Surprise! Inspire!');
        $I->see('How it works?');
        $I->see('Reasons for sending');
        $I->see('Why us?');
        $I->see('FAQ');
        $I->see('Reviews');
        $I->see('support@joymail.biz');
    }
}
