<?php

namespace App\Jobs;

use App\Http\Controllers\PaymentController;
use App\Modules\Payment\LiqPay;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PaymentProcess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    private $data;

    /**
     * Create a new job instance.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (empty($this->data)) {
            info('Data for payment is not recieved');
            return;
        }

        try {
            $orderId = LiqPay::savePayment($this->data);

            if ($orderId) {
                PaymentController::process($orderId, $this->data['site_url']);
            } else {
                throw new \Exception('Order ID is not found');
            }
        } catch (\Exception $e) {
            info('Payment is not saved: ' . $e->getMessage());
        }
    }
}
