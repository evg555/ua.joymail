<?
$offers = unserialize($model->offers);

if (empty($offers)) {
    return;
}

?>

@foreach ($offers as $name => $value)
    <div class="form-group form-element-text ">
        <label for="offers" class="control-label">{{$name}}</label>
        <input v-pre  class="form-control" type="text" id="offers" name="offers" value="{{$value}}" readonly>
    </div>
@endforeach
