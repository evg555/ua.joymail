<?php

namespace App\Http\Controllers;

use App\Helpers\OrderHelper;
use App\Jobs\PaymentProcess;
use Illuminate\Http\Request;

/**
 * Class PaymentController
 * @package App\Http\Controllers
 */
class PaymentController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('GET')) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Method GET is not supported. Use method POST'
            ]);
        }

        $data = $request->all();
        $data['site_url'] = $request->getSchemeAndHttpHost();

        info('ipay_payment_details: ' . serialize($data));

        PaymentProcess::dispatch($data);

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * @param int $orderId
     * @param string $siteUrl
     */
    public static function process(int $orderId, string $siteUrl = '')
    {
        $order = OrderHelper::getOrder($orderId);

        if (!$order instanceof OrderHelper) {
            return;
        }

        $order->setData('site_url', $siteUrl);

        $order->saveUser();
        $order->send('payed_order');
    }
}
