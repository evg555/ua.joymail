<?
Meta::loadPackage(['jquery']);
?>

<div class="form-group form-element-text ">
    <button class="btn btn-primary js-create-link">Create payment form</button>
</div>

<div class="form-group form-element-text payment-form"></div>

<script>
    $(function () {
        $('.js-create-link').on('click', function(e) {
            e.preventDefault();
            $(this).off();

            let targetForm = $('form.card');
            let DATA = new FormData(targetForm.get(0));

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "/ajax/create-form",
                type: "POST",
                dataType: "json",
                data: DATA,
                processData: false,
                contentType: false,
                success: function(answ) {
                    if (answ.status == 'success') {
                        $(answ.msg).appendTo($('.payment-form'));
                    } else {
                        Swal.fire({
                            title: 'Payment form is not created',
                            text: answ.msg,
                            icon: 'error',
                            confirmButtonText: 'OK'
                        });
                    }
                },
                error: function(xhr) {
                    if (xhr.responseJSON.errors) {
                        let errors = xhr.responseJSON.errors;
                        let errorMsg = '';

                        for (let key in errors) {
                            errorMsg += errors[key].join("\n") + "\n";
                        }

                        Swal.fire({
                            title: 'Payment form is not created',
                            text: errorMsg,
                            icon: 'error',
                            confirmButtonText: 'OK'
                        });
                    }
                }
            });

            return false;
        });
    });
</script>
