<?php

namespace App\Mail\Providers;

use Illuminate\Support\Facades\Mail;

/**
 * Class NewOrderProvider
 * @package App\Mail\Providers
 */
class NewOrderProvider extends MailProvider
{
    protected static $subject = 'New Order';

    /**
     * @param array $data
     */
    public static function handle(array $data)
    {
        static::send([
            'orders.new.seller' => static::getEmails(),
            'orders.new.customer' => $data['email_sender'] ?? ''
        ], $data);
    }
}
