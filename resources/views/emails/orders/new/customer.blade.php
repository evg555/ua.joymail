<h1>Уважаемый (ая)  {{$data['name_sender']}}.</h1>
<hr>
Спасибо за Ваш заказ #{{$data['order_id']}}!
<hr>
<font size=4px color="#FF0000">Общая сумма: {{$data['price']}} {{$data['currency']}}
<br> ! ВАШ ЗАКАЗ ОЖИДАЕТ ПОДТВЕРЖДЕНИЯ ОПЛАТЫ ! <br> В случае успешной оплаты, Вам придёт письмо-подтверждение.</font>
<hr>

<p>Детали заказа:</p>
Сообщение: {{$data['message']}}<br>
Город: {{$data['city']}}<br>
Промокод: {{$data['promocode']}}<br>
Имя отправителя: {{$data['name_sender']}}<br>
Тел. отправителя: {{$data['tel_sender']}}<br>
Имя получателя: {{$data['name_reciever']}}<br>
Тел. получателя: {{$data['tel_reciever']}}<br>

<hr>
Заказ:<br>
Тип открытки: {{$data['letter_type']}}<br>

@if (isset($data['offers']))
    @foreach ($data['offers'] as $name => $value)
        {{$name}}: {{$value}} pcs.<br>
    @endforeach
@endif

<p>
Ваша фотография находится здесь:
<br>
    @if (!empty($data['image']))
        <a href="{{$data['site_url']}}/{{$data['image']}}">Открыть фото</a>
    @endif
</p>
<hr>
Пожалуйста, не отвечайте на это письмо. Для связи с нами, напишите на ua-dnepr@joymail.biz.
<br>
Спасибо.
<p>С наилучшими пожеланиями, <br> Команда Joymail ))</p>

<hr>


<h1>Dear {{$data['name_sender']}}.</h1>
<hr>
Thank you for your order #{{$data['order_id']}}!
<hr>
<p>Details:</p>
Message: {{$data['message']}}<br>
City: {{$data['city']}}<br>
Promocode: {{$data['promocode']}}<br>
Sender name: {{$data['name_sender']}}<br>
Sender phone: {{$data['tel_sender']}}<br>
Receiver name: {{$data['name_reciever']}}<br>
Receiver Phone: {{$data['tel_reciever']}}<br>

<hr>
Additional items:<br>
Type of JoyKa: {{$data['letter_type']}}<br>

@if (isset($data['offers']))
    @foreach ($data['offers'] as $name => $value)
        {{$name}}: {{$value}} pcs.<br>
    @endforeach
@endif

<hr>
<font size=4px color="#FF0000">TOTAL AMOUNT: {{$data['price']}} {{$data['currency']}}
<br> ! YOUR ORDER IS AWAITING PAYMENT CONFIRMATION ! <br> Please wait for the confirmation e-mail.</font>
<hr>
Your uploaded picture is here:
<p>Photo:
    @if (!empty($data['image']))
        <a href="{{$data['site_url']}}/{{$data['image']}}">Open link</a>
    @endif
</p>
<hr>
Please do not reply for this e-mail. For any questions, please contact us ua-dnepr@joymail.biz
<br>
Thank you.
<p>Best Regards, <br> Team of Joymail LLC</p>

<img src="{{$data['site_url']}}/logo.png">
<hr>
