<?php

namespace App\Http\Controllers;

use App\Helpers\OrderHelper;
use App\Models\Offer;
use App\Modules\Payment\LiqPay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends Controller
{
    private $validatedData;

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        if ($request->hasFile('photo')) {
            $destination = 'upload/';

            $file = $request->file('photo');
            $filePath = 'img' . time() . '.' . $file->clientExtension();
            $file->move($destination, $filePath);
        }

        $offers = Offer::select('id')->get();

        $params = [
            'letter_type' => "max:255",
            'message' => "required",
            'city_id' => "required|integer",
            'promocode' => 'max:255',
            'name_sender' => "required|max:255",
            'name_reciever' => "required|max:255",
            'email_sender' => "required|email|max:255",
            'tel_sender' => "required|max:255",
            'tel_reciever'=> "required|max:255",
            'price' => 'required',
            'currency' => 'max:4',
            'agree' => 'accepted',
            'locale' => 'max:2'
        ];

        foreach ($offers as $offer) {
            $params['offer-' . $offer->id] = 'integer';
        }

        $data = $this->validate($request, $params);

        foreach ($data as $field => $value) {
            if (preg_match('#offer-#', $field) && !$value) {
                unset($data[$field]);
            }
        }

        if (!empty($filePath)) {
            $data['image'] = $destination . '/' . $filePath;
        }

        $data['site_url'] = $request->getSchemeAndHttpHost();
        $data['promocode'] = $data['promocode'] ?? '';

        $order = new OrderHelper($data);

        $data['order_id'] = $order->saveOrder();
        $data['offers'] = unserialize($order->proccessOfferFields());

        $order->send('new_order');

        if ($order->isNullPrice()) {
            $order->saveUser();

            $result = [
                'status' => 'success',
                'url' => $request->getSchemeAndHttpHost() . '/' . App::getLocale() . '/send?status=process'
            ];

            return response()->json($result);
        }

        $liqpay = new LiqPay(env('LIQPAY_PUBLIC_KEY'), env('LIQPAY_PRIVATE_KEY'));

        $result = $liqpay->getForm([
            'version' => 3,
            'action' => 'pay',
            'amount' => $data['price'],
            'currency' => $data['currency'],
            'description' => '',
            'order_id' => $data['order_id'],
            'language' => $data['locale'] == 'ua' ? 'uk' : $data['locale'],
            'result_url' => $request->getSchemeAndHttpHost() . '/' . $data['locale'] . '/send?status=process',
            'server_url' => $request->getSchemeAndHttpHost() . '/payment/'
        ]);

        if (!$result) {
            $result = ['status' => 'fail', 'msg' => 'Please contact to support'];
        } else {
            $result = [
                'status' => 'success',
                'html' => $result
            ];
        }

        return response()->json($result);
    }
}

