<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createOffers();
        $this->createOfferLangs();
    }

    private function createOffers()
    {
        DB::table('offers')->insert([
            'code' => 'flowers',
            'picture' => 'img/page2/ocassion-1.jpg',
            'price' => 5,
            'sort' => 1,
            'count' => 5,
            'created_at' => Carbon::now()
        ]);

        DB::table('offers')->insert([
            'code' => 'soft_toy',
            'picture' => 'img/page2/ocassion-2.jpg',
            'price' => 9,
            'sort' => 2,
            'count' => 1,
            'created_at' => Carbon::now()
        ]);

        DB::table('offers')->insert([
            'code' => 'mail',
            'picture' => 'img/page2/ocassion-3.jpg',
            'price' => 5,
            'sort' => 3,
            'count' => 5,
            'created_at' => Carbon::now()
        ]);

        DB::table('offers')->insert([
            'code' => 'rafaello',
            'price' => 13,
            'picture' => 'img/page2/ocassion-4.jpg',
            'sort' => 4,
            'count' => 5,
            'created_at' => Carbon::now()
        ]);

        DB::table('offers')->insert([
            'code' => 'gift',
            'picture' => 'img/page2/ocassion-5.jpg',
            'price' => 3,
            'sort' => 5,
            'count' => 5,
            'created_at' => Carbon::now()
        ]);
    }

    private function createOfferLangs()
    {
        DB::table('offer_langs')->insert([
            'lang' => 'en',
            'offer_id' => 1,
            'name' => 'Rose',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'en',
            'offer_id' => 2,
            'name' => 'Soft Toy',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'en',
            'offer_id' => 3,
            'name' => 'Helium Balloon',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'en',
            'offer_id' => 4,
            'name' => 'Rafaello',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'en',
            'offer_id' => 5,
            'name' => 'Chocolate',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'ge',
            'offer_id' => 1,
            'name' => 'ვარდი',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'ge',
            'offer_id' => 2,
            'name' => 'რბილი სათამაშო',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'ge',
            'offer_id' => 3,
            'name' => 'ბუშტი',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'ge',
            'offer_id' => 4,
            'name' => 'რაფაელო',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'ge',
            'offer_id' => 5,
            'name' => 'შოკოლადი',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'ru',
            'offer_id' => 1,
            'name' => 'Живая Роза',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'ru',
            'offer_id' => 2,
            'name' => 'Мягкая Игрушка',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'ru',
            'offer_id' => 3,
            'name' => 'Гелиевый Шарик',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'ru',
            'offer_id' => 4,
            'name' => 'Рафаэлло',
            'created_at' => Carbon::now()
        ]);

        DB::table('offer_langs')->insert([
            'lang' => 'ru',
            'offer_id' => 5,
            'name' => 'Шоколадка',
            'created_at' => Carbon::now()
        ]);
    }
}
