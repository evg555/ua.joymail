<?php

Route::get('', ['as' => 'admin.settings', function () {
    $content = 'Define your settings here.';
    return AdminSection::view($content, 'Settings');
}]);
