<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Feedback;
use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class MainController extends Controller
{
    public function index($locale = 'ua')
    {
        if ($locale) {
            App::setLocale($locale);
        }

        $feedbacks = Feedback::select(
            'feedbacks.id',
            'feedbacks.picture',
            'feedback_langs.body',
        )->join('feedback_langs', 'feedback_langs.feedback_id', '=', 'feedbacks.id')
            ->where('feedback_langs.lang','=', $locale)
            ->get();

        return view('main', compact('locale', 'feedbacks'));
    }

    public function send(Request $request, $locale = 'ua')
    {
        if ($locale) {
            App::setLocale($locale);
        }

        $cities = City::where('country_id', 2)->orderBy('sort', 'asc')->get();
        $offers = Offer::select(
                'offers.id',
                'offer_langs.name',
                'offers.code',
                'offers.picture',
                'offers.price',
                'offers.count'
            )->join('offer_langs', 'offer_langs.offer_id', '=', 'offers.id')
            ->where('offer_langs.lang','=', $locale)
            ->orderBy('offers.sort')
            ->get();



        $status = $request->get('status');

        return view('send', [
            'locale' => $locale,
            'cities' => $cities,
            'offers' => $offers,
            'status' => $status
        ]);
    }

    public function agreement($locale = 'ua')
    {
        if ($locale) {
            App::setLocale($locale);
        }

        return view('agreement', compact('locale'));
    }
}
