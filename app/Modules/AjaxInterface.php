<?php

namespace App\Modules;

use Illuminate\Http\Request;

/**
 * Interface AjaxInterface
 * @package App\Modules
 */
interface AjaxInterface
{
    /**
     * @param Request $request
     *
     * @return AjaxInterface
     */
    public function setParams(Request $request): AjaxInterface;

    public function dispatch(): array;
}
