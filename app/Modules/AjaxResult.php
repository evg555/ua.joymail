<?php

namespace App\Modules;

/**
 * Trait AjaxBase
 * @package App\Modules
 */
trait AjaxResult
{
    /**
     * @param string $message
     *
     * @return string[]
     */
    private function sendError(string $message = ''): array
    {
        return [
            'status' => 'error',
            'msg' => $message
        ];
    }

    /**
     * @param string $message
     * @param array $options
     *
     * @return string[]
     */
    private function sendSuccess(string $message = '', $options = []): array
    {
        $result = [
            'status' => 'success',
            'msg' => $message
        ];

        if (!empty($options)) {
            $result = array_merge($result, $options);
        }

        return $result;
    }
}
