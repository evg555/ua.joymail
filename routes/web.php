<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/{locale?}/', 'MainController@index');
Route::get('/{locale?}/send', 'MainController@send')->name('send');
Route::get('/send', function() {
    return response()->redirectTo('/ua/send');
});
Route::get('/{locale?}/agreement', 'MainController@agreement')->name('agreement');
Route::get('/agreement', function() {
    return response()->redirectTo('/ua/agreement');
});

Route::post('/send', 'OrderController@index');
Route::post('/ajax/{action}/', 'AjaxController@index');

Auth::routes();
