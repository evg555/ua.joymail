<?php

namespace App\Modules\Payment;

use App\Helpers\OrderHelper;
use App\Models\Payment;
use Exception;

/**
 * Class LiqPay
 * @package App\Helpers
 */
class LiqPay
{
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_USD = 'USD';
    const CURRENCY_UAH = 'UAH';
    const CURRENCY_RUB = 'RUB';
    const CURRENCY_RUR = 'RUR';

    private $checkoutUrl = 'https://www.liqpay.ua/api/3/checkout';
    protected $supportedCurrencies = [
        self::CURRENCY_EUR,
        self::CURRENCY_USD,
        self::CURRENCY_UAH,
        self::CURRENCY_RUB,
        self::CURRENCY_RUR,
    ];
    private $publicKey;
    private $privateKey;

    /**
     * Constructor.
     *
     * @param string $public_key
     * @param string $private_key
     * @param string $apiUrl (optional)
     *
     * @throws Exception
     */
    public function __construct($public_key, $private_key, $apiUrl = null)
    {
        if (empty($public_key)) {
            throw new Exception('public_key is empty');
        }

        if (empty($private_key)) {
            throw new Exception('private_key is empty');
        }

        $this->publicKey = $public_key;
        $this->privateKey = $private_key;

        if (null !== $apiUrl) {
            $this->apiUrl = $apiUrl;
        }
    }

    /**
     * cnb_form
     *
     * @param array $params
     *
     * @return string
     *
     * @throws Exception
     */
    public function getForm($params)
    {
        $language = 'ru';

        if (isset($params['language']) && $params['language'] == 'en') {
            $language = 'en';
        }

        $params = $this->prepareParams($params);
        $data = static::encodeParams($params);
        $signature = $this->getSignature($params);

        return sprintf('
            <form method="POST" action="%s" accept-charset="utf-8">
                %s
                %s
                <input type="image" src="//static.liqpay.ua/buttons/p1%s.radius.png" name="btn_text" />
            </form>
            ', $this->checkoutUrl,
            sprintf('<input type="hidden" name="%s" value="%s" />', 'data', $data),
            sprintf('<input type="hidden" name="%s" value="%s" />', 'signature', $signature),
            $language);
    }

    /**
     * cnb_form raw data for custom form
     *
     * @param $params
     *
     * @return array
     * @throws Exception
     */
    public function getFormRaw($params)
    {
        $params = $this->prepareParams($params);

        return [
            'url' => $this->checkoutUrl,
            'data' => static::encodeParams($params),
            'signature' => $this->getSignature($params)
        ];
    }

    /**
     * getSignature
     *
     * @param array $params
     *
     * @return string
     * @throws Exception
     */
    public function getSignature($params)
    {
        $params = $this->prepareParams($params);
        $private_key = $this->privateKey;

        $json = static::encodeParams($params);
        $signature = static::strToSign($private_key . $json . $private_key);

        return $signature;
    }

    /**
     * prepareParams
     *
     * @param array $params
     *
     * @return array $params
     * @throws Exception
     */
    private function prepareParams($params)
    {
        $params['public_key'] = $this->publicKey;

        if (!isset($params['version'])) {
            throw new Exception('version is null');
        }

        if (!isset($params['amount'])) {
            throw new Exception('amount is null');
        }

        if (!isset($params['currency'])) {
            throw new Exception('currency is null');
        }

        if (!in_array($params['currency'], $this->supportedCurrencies)) {
            throw new Exception('currency is not supported');
        }

        if ($params['currency'] == self::CURRENCY_RUR) {
            $params['currency'] = self::CURRENCY_RUB;
        }

        if (!isset($params['description'])) {
            throw new Exception('description is null');
        }

        return $params;
    }

    /**
     * encodeParams
     *
     * @param array $params
     *
     * @return string
     */
    public static function encodeParams($params)
    {
        return base64_encode(json_encode($params));
    }

    /**
     * @param $params
     *
     * @return false|string
     */
    private static function decodeParams($params)
    {
        return json_decode(base64_decode($params), true);
    }

    /**
     * strToSign
     *
     * @param string $str
     *
     * @return string
     */
    public static function strToSign($str)
    {
        $signature = base64_encode(sha1($str, 1));

        return $signature;
    }

    /**
     * @param array $data
     *
     * @return int|bool
     */
    public static function savePayment(array $data)
    {
        try {
            $decodeData = static::decodeParams($data['data']);

            $payment = new Payment();
            $payment->invoice_id = $decodeData['payment_id'];
            $payment->order_id = (int) $decodeData['order_id'];
            $payment->transaction_id = $decodeData['transaction_id'] ?: '';
            $payment->transaction_state = ($decodeData['status'] == 'success' ? 1 : 0);
            $payment->error = '';
            $payment->save();

            if ($decodeData['status'] == 'success') {
                OrderHelper::changeOrderStatus($payment->order_id);
                return $payment->order_id;
            }
        } catch (\Exception $e) {
            info('Payment is not saved: ' . $e->getMessage());
        }

        return false;
    }
}
