<?php

namespace App\Http\Admin;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use Illuminate\Http\UploadedFile;

/**
 * Class Order
 *
 * @property \App\Models\Order $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Offer extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fas fa-gift');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')
                ->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('code', 'Alt')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('code', 'like', '%'.$search.'%');
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('code', $direction);
                }),
            AdminColumn::image('picture', 'Image')
                ->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('sort', 'Sort')
                ->setHtmlAttribute('class', 'text-center')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('sort', $direction);
                }),
            AdminColumn::text('price', 'Price')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('price', '=', $search);
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('offers.price', $direction);
                }),
            AdminColumn::text('count', 'Count')
                ->setHtmlAttribute('class', 'text-center')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('count', $direction);
                }),
            AdminColumn::text('created_at', 'Created / updated', 'updated_at')
                ->setWidth('160px')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('updated_at', $direction);
                })
                ->setSearchable(false),
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'desc']])
            ->setDisplaySearch(true)
            ->paginate(50)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = AdminForm::card()->addBody([
            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('offers.id', 'ID')
                    ->setReadonly(true),
                AdminFormElement::html('<hr>'),
                AdminFormElement::text('code', 'Alt')->required(),
                AdminFormElement::number('sort', 'Sort')->setDefaultValue(1),
                AdminFormElement::number('price', 'Price')->required(),
                AdminFormElement::number('count', 'Count')->required(),
            ], 'col-xs-12 col-sm-4 col-md-4 col-lg-4')->addColumn([
                AdminFormElement::hasMany('lang', [
                    AdminFormElement::text('lang', 'Lang'),
                    AdminFormElement::text('name', 'Name'),
                ]),
            ], 'col-xs-12 col-sm-4 col-md-4 col-lg-4')->addColumn([
                AdminFormElement::image('picture', 'Image')->required()
                ->setUploadPath(function(UploadedFile $file) {
                    return 'img/ocassion';
                }),
            ], 'col-xs-12 col-sm-4 col-md-4 col-lg-4'),
        ]);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @param null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onCreate($id = null, $payload = [])
    {
        return $this->onEdit();
    }

    /**
     * @param Model $model
     *
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }
}
