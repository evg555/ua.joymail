@extends('layouts.app')

@section('content')
    <header class="header">
        <div class="container">
            <nav class="nav-mobile">
                <a class="logo" href="/{{$locale}}">
                    <img src="{{asset('img/logo.svg')}}" width="68" height="27" alt="logo">
                </a>
                <ul class="langs langs--mobile">
                <li class="langs__item">
                        <a class="langs__link" href="/ua">
                            <img src="{{asset('img/icons/ua.png')}}"
                                 width="40"
                                 height="40"
                                 alt="ua"
                                 aria-label="Switch to ua">
                        </a>
                    </li>
                    <li class="langs__item">
                        <a class="langs__link" href="/en">
                            <img src="img/icons/flag_1.svg')}}"
                                 width="40"
                                 height="40"
                                 alt="en"
                                 aria-label="Switch to en">
                        </a>
                    </li>
                    <li class="langs__item">
                        <a class="langs__link" href="/ru">
                            <img src="{{asset('img/icons/flag_2.svg')}}"
                                 width="40"
                                 height="40"
                                 alt="ru"
                                 aria-label="Switch to ru">
                        </a>
                    </li>

                </ul>
                <button class="cmn-toggle-switch cmn-toggle-switch__htx"
                        area-label="Open mobile nav"
                        id="menu__button">
                    <span>toggle menu</span>
                </button>
            </nav>
            <nav class="nav">
                <a class="logo" href="/{{$locale}}">
                    <img src="{{asset('img/logo.svg')}}" width="102" height="40" alt="logo">
                </a>

                <div class="countries">
                    <select name="country_id" onchange="location = this.value;">
                        <option value=""></a>@lang('main.country')</option>
                        <option value="https://joymail.biz/">@lang('main.country_ge')</option>
                        <option value="/{{$locale}}">@lang('main.country_ua')</option>
                    </select>
                </div>

                <ul class="nav-list nav-list--form">
                    <li class="nav__item"><a class="nav__link" href="#work">@lang('main.menu_1')</a></li>
                    <li class="nav__item"><a class="nav__link" href="#about">@lang('main.menu_2')</a></li>
                    <li class="nav__item"> <a class="nav__link" href="#reasons">@lang('main.menu_3')</a></li>
                    <li class="nav__item"><a class="nav__link" href="#faq">FAQ</a></li>
                    <li class="nav__item"><a class="nav__link" href="#reviews">@lang('main.menu_4')</a></li>
                </ul>

{{--                <a class="btn nav__btn" href="/{{$locale}}/send">@lang('main.send')</a>--}}

                <ul class="langs">
                                    <li class="langs__item">
                        <a class="langs__link" href="/ua">
                            <img src="{{asset('img/icons/ua.png')}}"
                                 width="40"
                                 height="40"
                                 alt="ua"
                                 aria-label="Switch to ua">
                        </a>
                    </li>

                    <li class="langs__item">
                        <a class="langs__link" href="/en">
                            <img src="{{asset('img/icons/flag_1.svg')}}"
                                 width="40"
                                 height="40"
                                 alt="en"
                                 aria-label="Switch on en">
                        </a>
                    </li>
                    <li class="langs__item">
                        <a class="langs__link" href="/ru">
                            <img src="{{asset('img/icons/flag_2.svg')}}"
                                 width="40"
                                 height="40"
                                 alt="ru"
                                 aria-label="Switch on ru">
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="header-content">
            <div class="container">
                <div class="header-content__left">
                    <h1 class="content__title">@lang('main.title')</h1>
                    <p class="content__subtitle">@lang('main.subtitle')</p>
                    <a class="btn content__btn" href="/{{$locale}}/send">@lang('main.send')</a>
                </div>
            </div>
        </div>
    </header>

    <section class="work-section" id="work">
        <div class="work-block"><span></span><span></span><span></span><span></span>
            <h2 class="work__title">@lang('main.menu_1')</h2>
            <div class="work-cards">
                <div class="item-card">
                    <div class="card__img"><img src="{{asset('img/icons/work_icon_1.svg')}}" width="308" height="233" alt="Человек делает селфи"></div>
{{--                    <div class="card__num card__num--first"></div> --}}
                    <h3 class="card__title">@lang('main.work_title_1')</h3>
                    <p class="card__text">@lang('main.work_text_1')</p>
                </div>
                <div class="item-card">
                    <div class="card__img card__img--mg card__img--lg"><img src="{{asset('img/icons/work_icon_2.svg')}}" width="294" height="208" alt="Конверт JoyKa с ярким фото"></div>
{{--                    <div class="card__num card__num--second"></div> --}}
                    <h3 class="card__title">@lang('main.work_title_2')</h3>
                    <p class="card__text">@lang('main.work_text_2')</p>
                </div>
                <div class="item-card">
                    <div class="card__img card__img--xs card__img--md"><img src="{{asset('img/icons/work_icon_3.svg')}}" width="292" height="216" alt="Курьер несет JoyKa"></div>
{{--                    <div class="card__num card__num--third"></div> --}}
                    <h3 class="card__title">@lang('main.work_title_3')</h3>
                    <p class="card__text">@lang('main.work_text_3')</p>
                </div>
                <div class="item-card">
                    <div class="card__img card__img--lg-last"><img src="{{asset('img/icons/work_icon_4.svg')}}" width="268" height="255" alt="Адресат радуется полученной JoyKa"></div>
{{--                    <div class="card__num card__num--fourth"></div> --}}
                    <h3 class="card__title">@lang('main.work_title_4')</h3>
                    <p class="card__text">@lang('main.work_text_4')</p>
                </div>
            </div>
        </div>
    </section>

    <section class="reasons-section" id="reasons">
        <h2 class="section__title reasons-section__title">@lang('main.menu_3')</h2>
        <div class="container">
            <div class="reasons">
                <div class="reasons-row">
                    <div class="reasons__card">
                        <div class="card__title">@lang('main.reason_text_1')</div>
                        <div class="card__text">@lang('main.reason_title_1')</div>
                    </div>
                    <div class="reasons__card">
                        <div class="card__title">@lang('main.reason_text_2')</div>
                        <div class="card__text">@lang('main.reason_title_2')</div>
                    </div>
                    <div class="reasons__card">
                        <div class="card__title">@lang('main.reason_text_3')</div>
                        <div class="card__text">@lang('main.reason_title_3')</div>
                    </div>
                    <div class="reasons__card">
                        <div class="card__title">@lang('main.reason_text_4')</div>
                        <div class="card__text">@lang('main.reason_title_4')</div>
                    </div>
                </div>
                <div class="reasons-row">
                    <div class="reasons__card">
                        <div class="card__title">@lang('main.reason_text_5')</div>
                        <div class="card__text">@lang('main.reason_title_5')</div>
                    </div>
                    <div class="reasons__card">
                        <div class="card__title">@lang('main.reason_text_6')</div>
                        <div class="card__text">@lang('main.reason_title_6')</div>
                    </div>
                    <div class="reasons__card">
                        <div class="card__title">@lang('main.reason_text_7')</div>
                        <div class="card__text">@lang('main.reason_title_7')</div>
                    </div>
                    <div class="reasons__card">
                        <div class="card__title">@lang('main.reason_text_8')</div>
                        <div class="card__text">@lang('main.reason_title_8')</div>
                    </div>
                </div>
            </div>
        </div><p> &nbsp </p><p> &nbsp </p>
                <div class="btn-row"><a class="btn reviews__btn" href="/{{$locale}}/send">@lang('main.send')</a></div>
    </section>

    <section class="about-section" id="about">
        <h2 class="section__title about-section__title">@lang('main.menu_2')</h2>
        <div class="container">
            <div class="about">
                <div class="about-row">
                    <div class="about__card about__card--first">
                        <div class="about__title">@lang('main.choose_title_1')</div>
                        <div class="about__text">@lang('main.choose_text_1')</div>
                    </div>
                    <div class="about__card about__card--second">
                        <div class="about__title">@lang('main.choose_title_2')</div>
                        <div class="about__text">@lang('main.choose_text_2')</div>
                    </div>
                    <div class="about__card about__card--third">
                        <div class="about__title">@lang('main.choose_title_3')</div>
                        <div class="about__text">@lang('main.choose_text_3')</div>
                    </div>
                    <div class="about__card about__card--fourth">
                        <div class="about__title">@lang('main.choose_title_4')</div>
                        <div class="about__text">@lang('main.choose_text_4')</div>
                    </div>
                </div>
                <div class="about-row">
                    <div class="about__card about__card--fifth">
                        <div class="about__title">@lang('main.choose_title_5')</div>
                        <div class="about__text">@lang('main.choose_text_5')</div>
                    </div>
                    <div class="about__card about__card--sixth">
                        <div class="about__title">@lang('main.choose_title_6')</div>
                        <div class="about__text">@lang('main.choose_text_6')</div>
                    </div>
                    <div class="about__card about__card--seventh">
                        <div class="about__title">@lang('main.choose_title_7')</div>
                        <div class="about__text">@lang('main.choose_text_7')</div>
                    </div>
                    <div class="about__card about__card--eights">
                        <div class="about__title">@lang('main.choose_title_8')</div>
                        <div class="about__text">@lang('main.choose_text_8')</div>
                    </div>
                </div>
            </div>
        </div><p> &nbsp </p><p> &nbsp </p>
                <div class="btn-row"><a class="btn reviews__btn" href="/{{$locale}}/send">@lang('main.send')</a></div>
    </section>

    <section class="faq-section" id="faq">
        <h2 class="section__title faq-section__title">FAQ</h2>
        <div class="container">
            <div class="accordion">
                <ul class="accordion-list">
                    <li class="accordion__item">
                        <h3 class="accordion__item-title">@lang('main.faq_title_1')</h3>
                        <p class="accordion__item-text">@lang('main.faq_text_1')</p>
                    </li>
                    <li class="accordion__item">
                        <h3 class="accordion__item-title">@lang('main.faq_title_2')</h3>
                        <p class="accordion__item-text">@lang('main.faq_text_2')</p>
                    </li>
                    <li class="accordion__item">
                        <h3 class="accordion__item-title">@lang('main.faq_title_3')</h3>
                        <p class="accordion__item-text">@lang('main.faq_text_3')</p>
                    </li>
                    <li class="accordion__item">
                        <h3 class="accordion__item-title">@lang('main.faq_title_4')</h3>
                        <p class="accordion__item-text">@lang('main.faq_text_4')</p>
                    </li>
                    <li class="accordion__item">
                        <h3 class="accordion__item-title">@lang('main.faq_title_5')</h3>
                        <p class="accordion__item-text">@lang('main.faq_text_5')</p>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <section class="reviews-section" id="reviews">
        <h2 class="section__title reviews-section__title">@lang('main.menu_4')</h2>
        <div class="reviews-slider">
            <div class="swiper-container reviews">
                <div class="swiper-wrapper">
                    @foreach ($feedbacks as $feedback)
                        <div class="swiper-slide review">
                            <div class="slider__img"><img src="{{$feedback->picture}}" width="331" height="404" alt="Review"></div>
                            <div class="slider__text">{{$feedback->body}}</div>
                        </div>
                   @endforeach
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
        <div class="btn-row"><a class="btn reviews__btn" href="/{{$locale}}/send">@lang('main.send')</a></div>
    </section>
@endsection

@section('footer')
    <footer class="footer footer--main">
        <div class="container">
            <div class="footer-cols">
                <div class="col__left">
                    <ul class="contacts-list">
                        <li class="contacts__item contacts__item--first">
                            <a class="contacts__link" href="tel:+380636629669">+380 63 662 9669</a>
                            <a class="contacts__link" href="tel:+380952301677">+380 95 230 1677</a>
                        </li>
                        <li class="contacts__item contacts__item--second"><a class="contacts__link" href="mailto:ua-dnepr@joymail.biz">ua-dnepr@joymail.biz</a><a class="contacts__link" href="mailto:support@joymail.biz">support@joymail.biz</a></li>
                        <li class="contacts__item contacts__item--third"><span class="contacts__link">Dnipro, Kirgizskaya str., 3/29. SP Fedorenko N.O.</span></li>
                    </ul>
                    <ul class="socials-list">
                        <li class="socials__item"><a class="socials__link" target="_blank" href="https://www.facebook.com/groups/2389460657958736"><img src="{{asset('/img/icons/facebook.svg')}}" width="36" height="36" alt="Фейсбук" aria-label="Фейсбук"></a></li>
                        <li class="socials__item"><a class="socials__link" target="_blank" href="https://www.instagram.com/joymail.biz/"><img src="{{asset('/img/icons/instagram.svg')}}" width="36" height="36" alt="Инстаграм" aria-label="Инстаграм"></a></li>
                        <li class="socials__item"><a class="socials__link" target="_blank" href="https://www.youtube.com/channel/UCvzXZmwFkhq6JT3c0acS4ZA?view_as=subscriber"><img src="{{asset('/img/icons/youtube.svg')}}" width="36" height="36" alt="Ютуб" aria-label="Ютуб"></a></li>
                    </ul>
                </div>

                <div class="col__right">
                    <a class="logo logo--footer" href="index.html">
                        <img src="{{asset('/img/logo.svg')}}" width="165" height="65" alt="Логотип джойка">
                    </a>
                </div>
            </div>

            <div class="col__center">
                <p class="col__right-slogan">
                    <span class="orange">@lang('footer.slogan1')</span> - <span class="blue">@lang('footer.slogan2')</span>
                </p>
            </div>
                      <div class="payment-labels">
                        <img src="/img/page2/liqpay.jpg" title="LiqPay invoice by E-mail">
                        <img src="/img/page2/visa.png" title="Visa">
                        <img src="/img/page2/master-card.png" title="Mastercard">
                    </div>
<a class="col__center" style="color:#FFFFFF; text-decoration: underline;" href="/{{$locale}}/agreement">@lang('main.link_agree')</a>
<div class="col__center" style="color:#FFFFFF; text-decoration: none;">
ФОП Федоренко Надiя Олександрiвна. Р/р UA593052990000026000050274339
            </div>
        </div>
    </footer>
@endsection
