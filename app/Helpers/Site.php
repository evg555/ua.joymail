<?php

namespace App\Helpers;

/**
 * Class Site
 * @package App\Helpers
 */
class Site
{
    public static function isTestEnv(): bool
    {
        return app()->environment() === 'test';
    }
}
