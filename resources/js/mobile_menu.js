$(function() {
    const hamburger = $('#menu__button');
    const menu = $('.nav');
    const menuLinks = $('.nav__link');
    const body = $('body');

    menuLinks.on('click', function() {
        hamburger.removeClass('active');
        menu.removeClass('nav-list--open');
        body.removeClass('no-scroll');
    });

    hamburger.on('click', function() {
        if (!hamburger.hasClass('active')) {
            hamburger.addClass('active');
            menu.addClass('nav-list--open');
            body.addClass('no-scroll');
        } else {
            hamburger.removeClass('active');
            menu.removeClass('nav-list--open');
            body.removeClass('no-scroll');
        }
    });

    window.addEventListener('resize', function() {
        screenWidth = window.screen.availWidth;

        if (screenWidth > 1024) {
            hamburger.removeClass('active');
            menu.removeClass('nav--open');
            body.removeClass('no-scroll');
        }
    });

});
