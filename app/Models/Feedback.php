<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedbacks';

    public function lang()
    {
        return $this->hasMany(FeedbackLang::class);
    }
}
