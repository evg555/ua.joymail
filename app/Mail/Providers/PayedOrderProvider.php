<?php

namespace App\Mail\Providers;

/**
 * Class PayedOrderProvider
 * @package App\Mail\Providers
 */
class PayedOrderProvider extends MailProvider
{
    protected static $subject = 'Payed Order';

    /**
     * @param array $data
     */
    public static function handle(array $data)
    {
        static::send([
            'orders.payed.seller' => static::getEmails(),
            'orders.payed.customer' => $data['email_sender'] ?? ''
        ], $data);
    }
}
