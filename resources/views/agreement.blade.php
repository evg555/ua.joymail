@extends('layouts.app')

@section('content')
    <div class="container">
        <nav class="nav-mobile"><a class="logo" href="/{{$locale}}">
                <img src="{{asset('/img/logo.svg')}}" width="68" height="27" alt="logo"></a>
            <ul class="langs langs--mobile">
                <li class="langs__item"><a class="langs__link" href="/ua/send">
                        <img src="{{asset('/img/icons/ua.png')}}"
                             width="40"
                             height="40"
                             alt="ua"
                             aria-label="Switch to UA">
                    </a>
                </li>

                <li class="langs__item"><a class="langs__link" href="/en/agreement">
                        <img src="{{asset('/img/icons/flag_1.svg')}}"
                             width="40"
                             height="40"
                             alt="en"
                             aria-label="Switch to EN">
                    </a>
                </li>
                <li class="langs__item"><a class="langs__link" href="/ru/agreement">
                        <img src="{{asset('/img/icons/flag_2.svg')}}"
                             width="40"
                             height="40"
                             alt="ru"
                             aria-label="Switch to RU">
                    </a>
                </li>
            </ul>
            <button class="cmn-toggle-switch cmn-toggle-switch__htx"
                    id="menu__button">
                <span>toggle menu</span>
            </button>
        </nav>

        <nav class="nav">
            <a class="logo" href="/{{$locale}}">
                <img src="{{asset('/img/logo.svg')}}" width="102" height="40" alt="logo">
            </a>

            <ul class="langs">
                <li class="langs__item">
                    <a class="langs__link" href="/ua/agreement">
                        <img src="{{asset('/img/icons/ua.png')}}"
                             width="40"
                             height="40"
                             alt="ua"
                             aria-label="Switch on ua">
                    </a>
                </li>

                <li class="langs__item">
                    <a class="langs__link" href="/en/agreement">
                        <img src="{{asset('/img/icons/flag_1.svg')}}"
                             width="40"
                             height="40"
                             alt="en"
                             aria-label="Switch on en">
                    </a>
                </li>
                <li class="langs__item">
                    <a class="langs__link" href="/ru/agreement">
                        <img src="{{asset('/img/icons/flag_2.svg')}}"
                             width="40"
                             height="40"
                             alt="ru"
                             aria-label="Switch on ru">
                    </a>
                </li>
            </ul>
        </nav>
<p>
</p><br>
        @lang('send.agreement_page')
    </div>
  <br>   <br>
<div class="btn-row"><a class="btn reviews__btn" href="/{{$locale}}/send">@lang('main.send')</a></div>
  <br>   <br>
@endsection

@section('footer')
    <footer class="footer footer--main">
        <div class="container">
            <div class="footer-cols">
                <div class="col__left">
                    <ul class="contacts-list">
                        <li class="contacts__item contacts__item--first">
                            <a class="contacts__link" href="tel:+380636629669">+380 63 662 9669</a>
                            <a class="contacts__link" href="tel:+380952301677">+380 95 230 1677</a>
                        </li>
                        <li class="contacts__item contacts__item--second"><a class="contacts__link" href="mailto:ua-dnepr@joymail.biz">ua-dnepr@joymail.biz</a><a class="contacts__link" href="mailto:support@joymail.biz">support@joymail.biz</a></li>
                        <li class="contacts__item contacts__item--third"><span class="contacts__link">Dnipro, Kirgizskaya str., 3/29. SP Fedorenko N.O.</span></li>
                    </ul>
                    <ul class="socials-list">
                        <li class="socials__item"><a class="socials__link" target="_blank" href="https://www.facebook.com/groups/2389460657958736"><img src="{{asset('/img/icons/facebook.svg')}}" width="36" height="36" alt="Фейсбук" aria-label="Фейсбук"></a></li>
                        <li class="socials__item"><a class="socials__link" target="_blank" href="https://www.instagram.com/joymail.biz/"><img src="{{asset('/img/icons/instagram.svg')}}" width="36" height="36" alt="Инстаграм" aria-label="Инстаграм"></a></li>
                        <li class="socials__item"><a class="socials__link" target="_blank" href="https://www.youtube.com/channel/UCvzXZmwFkhq6JT3c0acS4ZA?view_as=subscriber"><img src="{{asset('/img/icons/youtube.svg')}}" width="36" height="36" alt="Ютуб" aria-label="Ютуб"></a></li>
                    </ul>
                </div>

                <div class="col__right">
                    <a class="logo logo--footer" href="index.html">
                        <img src="{{asset('/img/logo.svg')}}" width="165" height="65" alt="Логотип джойка">
                    </a>
                </div>
            </div>

            <div class="col__center">
                <p class="col__right-slogan">
                    <span class="orange">@lang('footer.slogan1')</span> - <span class="blue">@lang('footer.slogan2')</span>
                </p>
            </div>
            <div class="payment-labels">
                        <img src="/img/page2/liqpay.jpg" title="LiqPay invoice by E-mail">
                        <img src="/img/page2/visa.png" title="Visa">
                        <img src="/img/page2/master-card.png" title="Mastercard">
            </div>
<a class="col__center" style="color:#FFFFFF; text-decoration: underline;" href="/{{$locale}}/agreement">@lang('main.link_agree')</a>
<div class="col__center" style="color:#FFFFFF; text-decoration: none;">
ФОП Федоренко Надiя Олександрiвна. Р/р UA593052990000026000050274339
            </div>
        </div>
    </footer>
@endsection
