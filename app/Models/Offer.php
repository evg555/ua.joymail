<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    public function lang()
    {
        return $this->hasMany(OfferLang::class);
    }
}
