<?php

namespace App\Providers;


use App\Models\Feedback;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Promocode;
use App\Models\User;
use KodiCMS\Assets\Facades\PackageManager;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;
use SleepingOwl\Admin\Admin;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        Order::class => 'App\Http\Admin\Order',
        Payment::class => 'App\Http\Admin\Payment',
        Promocode::class => 'App\Http\Admin\Promocode',
        Offer::class => 'App\Http\Admin\Offer',
        Feedback::class => 'App\Http\Admin\Feedback',
        //User::class => 'App\Http\Admin\Users',
    ];

    /**
     * Register sections.
     *
     * @param \SleepingOwl\Admin\Admin $admin
     * @return void
     */
    public function boot(Admin $admin)
    {
        PackageManager::add('jquery')
            ->js('jquery.js', 'https://code.jquery.com/jquery-3.1.0.min.js');

        parent::boot($admin);
    }
}
