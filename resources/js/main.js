import Swal from 'sweetalert2';

$(function () {
    let card = $('.first-row__select > .item-card'),
        uploadBtn = $('.upload__button'),
        uploadImg = $('.third-row-upload'),
        plus = $('.mdi-plus'),
        minus = $('.mdi-minus');

    minus.on('click', function() {
        let total = $(this).parents('.total-num').find('span'),
            amount = parseInt(total.text()),
            full = $(this).parents('.swiper-slide').find('.amount-full'),
            result = $(this).parents('.swiper-slide').find('.offer-count');

        if (amount > 0) {
            total.text(--amount);
            result.val(amount);
            full.val(0);
        } else {
            full.val(-1);
        }
    });

    plus.on('click', function() {
        let total = $(this).parents('.total-num').find('span'),
            amount = parseInt(total.text()),
            limit = parseInt($(this).parents('.swiper-slide').find('.amount-limit').val()),
            full = $(this).parents('.swiper-slide').find('.amount-full'),
            result = $(this).parents('.swiper-slide').find('.offer-count');

        if (amount < limit) {
            total.text(++amount);
            result.val(amount);
            full.val(0);
        } else {
            full.val(1);
        }
    });

    card.on('click', function() {
        card.removeClass('card-active');
        $(this).addClass('card-active');
    });

    uploadBtn.on('change', function(e) {
        if (this.files[0]) {
            let file = this.files[0];
            let allowedMimeTypes = [
                'image/jpeg',
                'image/jpg',
                'image/png'
            ];
            let fr = new FileReader();

            if (file.size > 10000000) {
                this.value = '';

                Swal.fire({
                    icon: 'error',
                    text: 'Image size must be less than 10Mb',
                    confirmButtonText: 'OK'
                });

                return false;
            }

            if (!allowedMimeTypes.includes(file.type)) {
                this.value = '';

                Swal.fire({
                    icon: 'error',
                    text: 'Allowed images types are jpg, jpeg, png',
                    confirmButtonText: 'OK'
                });

                return false;
            }

            fr.addEventListener("load", function () {
                uploadImg.find('img').hide();
                uploadImg.find('.upload__text').hide();

                uploadImg.css('background-size', 'cover');
                uploadImg.css('background-image', 'url(' + fr.result + ')');
                uploadImg.css('background-position', 'center center');
            }, false);

            fr.readAsDataURL(file);
        }
    });

    //Приклееная кнопка в мобильной версии
    $(document).on('scroll', function() {
        if (pageYOffset >= 485 && window.screen.width <= 768) {
            $('.content__btn').addClass('btn-fix');
            $('.btn-row').hide();
        } else {
            $('.btn-row').show();
            $('.content__btn').removeClass('btn-fix');
        }
    });

    /* @calculator.js*/
    calculator.init();
});
