<?php


use App\Models\Order;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use App\Modules\Payment\LiqPay;

class TransactionCest
{
    public function _before(FunctionalTester $I)
    {
        DB::table('jobs')->truncate();
        DB::table('payments')->where('invoice_id', '=', 'functional_test')->delete();
    }

    public function payment(FunctionalTester $I)
    {
        $order = Order::select('id')->first();

        $data = LiqPay::encodeParams([
            'payment_id' => 'functional_test',
            'order_id' => $order->id,
            'transaction_id' => 'functional_test',
            'status' => 'success'
        ]);

        $I->sendPost('/payment/', [
            'data' => $data
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson(['status' => 'success']);
        $I->seeInDatabase('jobs',['id' => 1]);

        //$exitCode = Artisan::call('queue:work', ['--once']);
        //$I->seeInDatabase('payments', ['invoice_id' => 'functional_test']);
        //$I->seeInDatabase('orders', ['status' => 'P']);
    }

}
