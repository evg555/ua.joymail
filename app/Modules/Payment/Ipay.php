<?php

namespace App\Modules\Payment;

use App\Helpers\Http;
use App\Helpers\OrderHelper;
use App\Models\Payment;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;
use Psy\Util\Json;

/**
 * Class Ipay
 * @package App\Modules\Payment
 */
class Ipay
{
    /**
     * @param array $order
     *
     * @return false|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function createPaymentLink(array $order): string
    {
        $locale = App::getLocale() == 'ge' ? 'ge' : 'en';
        $http = Http::getSchemaProtocol();
        $paymentLink = '';

        $token = static::getToken();

        $data = [
            "intent" => "CAPTURE",
            "redirect_url" => $http . $_SERVER['HTTP_HOST'] . "/{$locale}/send?status=process",
            "shop_order_id" => (string) $order['order_id'],
            "card_transaction_id" => '',
            "locale" => $locale == 'ge' ? 'ka' : 'en-US',
            "purchase_units" => [
                [
                    "amount" => [
                        "currency_code" => $order['currency'],
                        "value" => (string) $order['price']
                    ],
                    "industry_type" => "ECOMMERCE"
                ]
            ],
            "items" => []
        ];

        $client = new Client();
        $response = $client->request('POST', \env('IPAY_API_URL') . 'checkout/orders/', [
            'body' => Json::encode($data),
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token
            ]
        ]);

        $result = json_decode($response->getBody(),true);

        if ($result['status'] == 'CREATED') {
            foreach ($result['links'] as $link) {
                if ($link['rel'] == 'approve') {
                    $paymentLink = $link['href'] ?? '';
                    break;
                }
            }
        }

        return $paymentLink;
    }

    /**
     * @param array $data
     *
     * @return int|bool
     */
    public static function savePayment(array $data)
    {
        try {
            $payment = new Payment();
            $payment->invoice_id = $data['order_id'];
            $payment->order_id = (int) $data['shop_order_id'];
            $payment->transaction_id = $data['ipay_payment_id'] ?: '';
            $payment->transaction_state = ($data['status'] == 'success' ? 1 : 0);
            $payment->error = $data['status_description'] ?: '';
            $payment->save();

            if ($data['status'] == 'success') {
                OrderHelper::changeOrderStatus($payment->order_id);
                return $payment->order_id;
            }
        } catch (\Exception $e) {
            info('Payment is not saved: ' . $e->getMessage());
        }

        return false;
    }

    private static function getToken(): string
    {
        $data = [
            'grant_type' => 'client_credentials',
            'client_id' => env('IPAY_CLIENT_ID'),
            'client_secret' => env('IPAY_SECRET_KEY')
        ];

        $client = new Client();
        $response = $client->request('POST', env('IPAY_API_URL') . 'oauth2/token', [
            'form_params' => $data,
            'auth' => [env('IPAY_CLIENT_ID'), env('IPAY_SECRET_KEY')]
        ]);

        $result = json_decode($response->getBody(),true);

        return $result['access_token'] ?? '';
    }
}
