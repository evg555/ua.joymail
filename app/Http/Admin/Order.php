<?php

namespace App\Http\Admin;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Models\Order as OrderEntity;

/**
 * Class Order
 *
 * @property \App\Models\Order $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Order extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('far fa-envelope');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('id', 'like', '%'.$search.'%');
                }),
            AdminColumn::text('status', 'Status')
                ->setOrderable(function($column, $query, $direction) {
                    $query->orderBy('status', $direction);
                }),
            AdminColumn::link('name_sender', 'Sender\'s name')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('name_sender', 'like', '%'.$search.'%');
                })
                ->setOrderable(function($column, $query, $direction) {
                    $query->orderBy('name_sender', $direction);
                }),
            AdminColumn::text('email_sender', 'Sender\'s email')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('email_sender', 'like', '%'.$search.'%');
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('email_sender', $direction);
                }),
            AdminColumn::text('tel_sender', 'Sender\'s phone')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('tel_sender', 'like', '%'.$search.'%');
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('tel_sender', $direction);
                }),
            AdminColumn::text('price', 'Price')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('price', 'like', '%'.$search.'%');
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('price', $direction);
                }),
            AdminColumn::text('created_at', 'Created / updated', 'updated_at')
                ->setWidth('160px')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('updated_at', $direction);
                })
                ->setSearchable(false),
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'desc']])
            ->setDisplaySearch(true)
            ->paginate(50)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = AdminForm::card()->addBody([
            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('id', 'ID')
                    ->setReadonly(true),
                AdminFormElement::html('<hr>'),
                AdminFormElement::select('status', 'Status', [
                    OrderEntity::ORDER_STATUS_NEW => 'New',
                    OrderEntity::ORDER_STATUS_PAYED => 'Payed'
                ])->required(),
                AdminFormElement::text('name_sender', 'Sender\'s name')->required(),
                AdminFormElement::text('email_sender', 'Sender\'s email')->required(),
                AdminFormElement::text('tel_sender', 'Sender\'s phone')->required(),
                AdminFormElement::text('name_reciever', 'Reciever\'s name')->required(),
                AdminFormElement::text('tel_reciever', 'Reciever\'s phone')->required(),
                //AdminFormElement::text('address', 'Address')->required(),
                AdminFormElement::html('<hr>'),
                AdminFormElement::text('promocode', 'Applied promocode')->setReadonly(true),
                AdminFormElement::number('price', 'Price')->setReadonly(true),
                AdminFormElement::text('currency', 'Currency')->setReadonly(true),
                AdminFormElement::html('<hr>'),
                AdminFormElement::datetime('created_at')
                    ->setVisible(true)
                    ->setReadonly(true)
                ,
            ], 'col-xs-12 col-sm-6 col-md-4 col-lg-4')->addColumn([
                AdminFormElement::textarea('message', 'Message')->required(),
                AdminFormElement::image('photo'),
                AdminFormElement::html('<hr>'),
                AdminFormElement::text('letter_type', 'Letter type')->setReadonly(true),
                AdminFormElement::view('admin.offers', $data = [], function() {
                    //
                }),
                AdminFormElement::html('<hr>'),
                AdminFormElement::view('admin.payment', $data = [], function() {
                    //
                })
            ], 'col-xs-12 col-sm-6 col-md-8 col-lg-8'),
        ]);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @param null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onCreate($id = null, $payload = [])
    {
        $form = AdminForm::card()->addBody([
            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('id', 'ID')
                    ->setReadonly(true),
                AdminFormElement::html('<hr>'),
                AdminFormElement::select('city_id', 'City', [
                    '2' => 'Dnipro',
                ])->required(),
                AdminFormElement::text('name_sender', 'Sender\'s name')->required(),
                AdminFormElement::text('email_sender', 'Sender\'s email')->required(),
                AdminFormElement::text('tel_sender', 'Sender\'s phone')->required(),
                AdminFormElement::text('name_reciever', 'Reciever\'s name')->required(),
                AdminFormElement::text('tel_reciever', 'Reciever\'s phone')->required(),
                //AdminFormElement::text('address', 'Address')->required(),
                AdminFormElement::html('<hr>'),
                AdminFormElement::number('price', 'Price')->required(),
                AdminFormElement::select('currency', 'Currency', [
                    'UAH' => 'UAH ',
                ])->required(),
                AdminFormElement::html('<hr>'),
            ], 'col-xs-12 col-sm-6 col-md-4 col-lg-4')->addColumn([
                AdminFormElement::textarea('message', 'Message')->required(),
                AdminFormElement::image('photo'),
                AdminFormElement::html('<hr>'),
                AdminFormElement::select('letter_type', 'Letter type', [
                    'love-letter' => 'Love Letter',
                    'standart' => 'Standart'
                ])->required(),
            ], 'col-xs-12 col-sm-6 col-md-8 col-lg-8'),
        ]);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @param Model $model
     *
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }
}
