<?php

namespace App\Helpers;

use App\Mail\Providers\NewOrderProvider;
use App\Mail\Providers\PayedOrderProvider;
use App\Models\City;
use App\Models\Offer;
use App\Models\Order;
use App\Models\User;
use Illuminate\Support\Str;

class OrderHelper
{
    const LETTER_TYPES = [
        'standart' => 'Standard',
        'love-letter' => 'Love Letter'
    ];

    const MAIL_PROVIDERS = [
        'new_order' => NewOrderProvider::class,
        'payed_order' => PayedOrderProvider::class
    ];

    private $order;

    public function setData($key, $value)
    {
        $this->order[$key] = $value;
    }

    public function __construct(array $data)
    {
        $this->order = $data;
    }


    public function saveUser()
    {
        $existUser = User::where('email', '=', $this->order['email_sender'])->count() > 0;

        if (!$existUser) {
            $user = new User();
            $user->name = $this->order['name_sender'];
            $user->email = $this->order['email_sender'];
            $user->password = password_hash(time(), PASSWORD_BCRYPT, ['cost' => 10]);;
            $user->save();
        }
    }

    /**
     * @param $status
     *
     * @return void
     */
    public function send($status)
    {
        if (empty($this->order)) {
            return;
        }

        if (array_key_exists($this->order['letter_type'], static::LETTER_TYPES)) {
            $this->order['letter_type'] = static::LETTER_TYPES[$this->order['letter_type']];
        } else {
            $this->order['letter_type'] = 'Default';
        }

        $this->order['city'] = City::getCityName($this->order['city_id']);

        $mailProvider = static::MAIL_PROVIDERS[$status];
        $mailProvider::handle($this->order);
    }

    /**
     * @param int $orderId
     *
     * @return mixed
     */
    public static function getOrder(int $orderId)
    {
        $order = Order::where('id', $orderId)->get()->toArray();

        if (empty($order)) {
            info('Order with id ' . $orderId . ' is not found');
            return null;
        }

        $order = current($order);
        $order['offers'] = unserialize($order['offers']);
        $order['order_id'] = $order['id'];

        return new static($order);
    }

    public function saveOrder()
    {
        $order = new Order();
        $order->fill($this->order);
        $order->photo = $this->order['image'] ?? '';
        $order->offers = $this->proccessOfferFields();
        $order->save();

        $this->order['order_id'] = $order->id;

        return $order->id;
    }

    public function proccessOfferFields(): string
    {
        $offerFields = array_filter(array_keys($this->order), function($field) {
            return strpos($field, 'offer') !== false;
        });

        $id = [];

        foreach ($offerFields as $offerField) {
            $id[] = (int) Str::after($offerField, 'offer-');
        }

        if (empty($id)) {
            return serialize([]);
        }

        $offers = Offer::select(
            'offers.id',
            'offer_langs.name'
        )->join('offer_langs', 'offer_langs.offer_id', '=', 'offers.id')
            ->where('offer_langs.lang','=', 'en')
            ->whereIn('offers.id', $id)
            ->orderBy('offers.sort')
            ->get();

        $fields = [];

        foreach ($offers as $offer) {
            $fields[$offer->name] = $this->order['offer-' . $offer->id];
        }

        $this->order['offers'] = $fields;

        return serialize($fields);
    }

    /**
     * @return bool
     */
    public function isNullPrice()
    {
        return $this->order['price'] < 1;
    }

    /**
     * @param int $orderId
     */
    public static function changeOrderStatus(int $orderId)
    {
        $order = Order::find($orderId);
        $order->status = Order::ORDER_STATUS_PAYED;
        $order->save();
    }
}
