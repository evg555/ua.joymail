@extends('layouts.app')

@section('content')

<header class="header header--form">
    <div class="container">
        <nav class="nav-mobile"><a class="logo" href="/{{$locale}}">
                <img src="{{asset('/img/logo.svg')}}" width="68" height="27" alt="logo"></a>
            <ul class="langs langs--mobile">
                            <li class="langs__item"><a class="langs__link" href="/ua/send">
                        <img src="{{asset('/img/icons/ua.png')}}"
                             width="40"
                             height="40"
                             alt="ua"
                             aria-label="Switch to UA">
                    </a>
                </li>

                <li class="langs__item"><a class="langs__link" href="/en/send">
                        <img src="{{asset('/img/icons/flag_1.svg')}}"
                             width="40"
                             height="40"
                             alt="en"
                             aria-label="Switch to EN">
                    </a>
                </li>
                <li class="langs__item"><a class="langs__link" href="/ru/send">
                        <img src="{{asset('/img/icons/flag_2.svg')}}"
                             width="40"
                             height="40"
                             alt="ru"
                             aria-label="Switch to RU">
                    </a>
                </li>
            </ul>
            <button class="cmn-toggle-switch cmn-toggle-switch__htx"
                    id="menu__button">
                <span>toggle menu</span>
            </button>
        </nav>

        <nav class="nav">
            <a class="logo" href="/{{$locale}}">
                <img src="{{asset('/img/logo.svg')}}" width="102" height="40" alt="logo">
            </a>

            <div class="countries">
                <select name="country_id" onchange="location = this.value;">
                    <option value=""></a>@lang('main.country')</option>
                    <option value="https://joymail.biz/send">@lang('main.country_ge')</option>
                    <option value="/{{$locale}}/send">@lang('main.country_ua')</option>
                </select>
            </div>

            <ul class="langs">
                            <li class="langs__item">
                    <a class="langs__link" href="/ua/send">
                        <img src="{{asset('/img/icons/ua.png')}}"
                             width="40"
                             height="40"
                             alt="ua"
                             aria-label="Switch on ua">
                    </a>
                </li>

                <li class="langs__item">
                    <a class="langs__link" href="/en/send">
                        <img src="{{asset('/img/icons/flag_1.svg')}}"
                             width="40"
                             height="40"
                             alt="en"
                             aria-label="Switch on en">
                    </a>
                </li>
                <li class="langs__item">
                    <a class="langs__link" href="/ru/send">
                        <img src="{{asset('/img/icons/flag_2.svg')}}"
                             width="40"
                             height="40"
                             alt="ru"
                             aria-label="Switch on ru">
                    </a>
                </li>
            </ul>
        </nav>

        <div class="header--form__top">
            <h1 class="header--form__title">@lang('send.create')</h1>
            <a class="logo--form" href="/{{$locale}}">
                <img src="{{asset('/img/logo.svg')}}" width="165" height="65" alt="logo">
            </a>
        </div>

        <form class="header--form-content" action="/send" id="data-form" method="post" enctype="multipart/form-data">
            <input type="hidden" class="calculator-input" name="price" value="">
            <input type="hidden" name="currency" value="UAH">
            <input type="hidden" name="locale" value="{{$locale}}">

            <div class="content-row first-row">
                <h2 class="row__title row__title--first">@lang('send.select')</h2>
                <div class="first-row__select">
                    <div class="calculator-card create-item item-card card-active" data-item="standart" data-value="120">
                        <div class="img-wrap">
                            <img src="{{asset('/img/white_joyka@1x.jpg')}}"
                                 width="256" height="206" alt="white joyka">
                            <div class="card-info">
                                <div class="name">@lang('send.cover_1')</div>
                                <div class="price">120 UAH</div>
                            </div>
                        </div>
                    </div>
                    <div class="calculator-card create-item item-card" data-item="love-letter" data-value="150">
                        <div class="img-wrap">
                            <img src="{{asset('/img/craft_joyka@1x.jpg')}}"
                                 width="256" height="206" alt="craft joyka">
                            <div class="card-info">
                                <div class="name">@lang('send.cover_2')</div>
                                <div class="price">150 UAH</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-row second-row">
                <h2 class="row__title row__title--second">@lang('send.message')</h2>
                <div class="row__title__subtitle">@lang('send.your_message')</div>
                <div class="second-row-message">
                    <textarea required
                              maxlength="300"
                              name="message"
                              placeholder="@lang('send.textarea')">{{old('message')}}</textarea>
                </div>
            </div>

            <div class="content-row third-row">
                <h2 class="row__title row__title--third">@lang('send.photo')</h2>
                <div class="third-row-upload">
                    <img src="{{asset('/img/icons/upload.svg')}}" width="65" height="72" alt="upload image">
                    <input id="photo" class="upload__button" type="file" name="photo">
                    <div class="upload__text">@lang('send.photo_click')</div>
                </div>
            </div>

            <div class="content-row fourth-row">
                <h2 class="row__title row__title--fourth">@lang('send.items_title')</h2>
                <div class="slider-wrap">
                    <div class="swiper-container fourth-row--additional">
                        <div class="swiper-wrapper">
                            @foreach ($offers as $offer)
                                <div class="swiper-slide item-card calculator-item">
                                    <div class="img-wrap">
                                        <img src="/{{$offer->picture}}" width="136" height="136" alt="{{$offer->code}}">
                                    </div>
                                    <div class="card-info">
                                        <div class="name">{{$offer->name}}</div>
                                        <div class="price"><span class="calculator-base-price">{{$offer->price}}</span> UAH</div>
                                    </div>
                                    <div class="total-price"><span class="calculator-onchange-total">0</span> UAH</div>
                                    <div class="total-num">
                                        <span class="calculator-onchange-sum">0</span>
                                        <div class="minus">
                                            <i class="mdi mdi-minus calculator-decrease"></i>
                                        </div>
                                        <div class="plus">
                                            <i class="mdi mdi-plus calculator-increase"></i>
                                        </div>
                                    </div>

                                    <input type="hidden" class="offer-count" name="offer-{{$offer->id}}" value="0">
                                    <input type="hidden" class="amount-limit" value="{{$offer->count}}">
                                    <input type="hidden" class="calculator-onchange-full  amount-full" value="0">
                                </div>
                            @endforeach

                                <div class="swiper-slide item-card calculator-item">
                                    <a href="#individual-order" data-toggle="modal">
                                        <div class="img-wrap">
                                            <img src="{{asset('/img/question.png')}}" width="136" height="136" alt="individual">
                                        </div>
                                    </a>
                                    <div class="card-info">
                                        <div class="name">@lang('send.custom_offer')</div>
                                    </div>
                                </div>
                        </div>

                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <div class="total-slider">@lang('send.total'):
                    <span class="total__sum-slider"><span class="calculator-result">0</span> UAH</span>
                </div>
            </div>

            <div class="content-row fifth-row">
                <div class="row__title row__title--fifth">@lang('send.form_title')</div>
                <div class="fifth-row-cols">
                    <div class="col col--first">
                        <div class="input-wrap">
                            <span class="select-title">@lang('send.input_1')</span>
                            <select name="city_id" class="select-city">
                                @foreach ($cities as $city)
                                    <option value="{{$city->id}}"
                                            class="data-city__option"
                                            @if ($city->id !== 2) disabled @endif>
                                        {{$city->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="input-wrap">
                            <label for="sender-email">@lang('send.input_9')</label>
                            <input id="sender-email" type="email" name="email_sender" value="{{old('email_sender')}}" required>
                        </div>

                    </div>
                    <div class="col col--second">
                        <div class="input-wrap">
                            <label for="senders-phone">@lang('send.input_5')</label>
                            <input id="senders-phone" type="text" name="tel_sender" value="{{old('tel_sender')}}" required>
                        </div>
                        <div class="input-wrap">
                            <label for="receiver-phone">@lang('send.input_7')</label>
                            <input id="receiver-phone" type="text" name="tel_reciever" value="{{old('tel_reciever')}}" required>
                        </div>
                    </div>
                    <div class="col col--third">
                        <div class="input-wrap">
                            <label for="senders-name">@lang('send.input_4')</label>
                            <input id="senders-name" type="text" name="name_sender" value="{{old('name_sender')}}" required>
                        </div>
                        <div class="input-wrap">
                            <label for="receiver-name">@lang('send.input_6')</label>
                            <input id="receiver-name" type="text" name="name_reciever" value="{{old('name_reciever')}}" required>
                        </div>
                        <div class="input-wrap">
                            <label for="promocode-phone">@lang('send.input_3')</label>
                            <div class="wrap">
                                <input id="promocode-phone" type="text" name="promocode" value="{{old('promocode')}}">
                                <span class="promocode__icon"></span>
                                <a class="btn btn__promo js-promocode-apply">@lang('send.check')</a>
                                <p class="warning promocode-alert">@lang('send.promocode_alert')</p>
                                <p class="success promocode-success">@lang('send.promocode_success')</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="content-row sixth-row">
                <div class="total">@lang('send.total'): <span class=" total__sum">
                    <span class="calculator-result">0</span> UAH</span>
                </div>
                <div class="wrap">
                     <button class="btn btn__pay" type="submit">@lang('send.pay')</button>
                     <div class="inner-wrap">
                        <input id="agreement" type="checkbox" name="agree" checked required>
                        <a class="policy-agreement" href="#agree" data-toggle="modal" >@lang('send.input_8')</a>
                    </div>

                    <div class="payment-labels">
                        <img src="/img/page2/liqpay.jpg" title="LiqPay invoice by E-mail">
                        <img src="/img/page2/visa.png" title="Visa">
                        <img src="/img/page2/master-card.png" title="Mastercard">
                        <img src="/img/page2/apppay.jpg" title="Apple Pay">
                        <img src="/img/page2/gpay.jpg" title="Google Pay">

                    </div>
                </div>
            </div>
        </form>

        <div id="agree" class="modal" tabindex="-1" role="dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">SP Fedorenko N.O. Agreement.</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="mdi mdi-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-agreement">
                        @lang('send.agreement_page')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary mx-auto d-block" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="individual-order" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('send.individual_form')</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="mdi mdi-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-agreement">
                            <form action="" method="post" id="data-form-individual">
                                <div class="form-group">
                                    <label for="phone">@lang('send.your_phone')</label>
                                    <input class="form-control" type="tel" name="phone" required>
                                </div>
                                <div class="form-group">
                                    <label for="message">@lang('send.your_message')</label>
                                    <textarea class="form-control" name="message" cols="30" rows="10"></textarea>
                                </div>

                                <button type="submit" class="btn btn-lg btn-primary">@lang('send.send')</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="preloader">
            <img src="{{asset('/img/preloader.gif')}}" alt="">
        </div>

        @if ($status)
            <script>
                $(document).ready(function() {
                    var status = '{{$status}}';

                    switch (status) {
                        case 'success':
                            Swal.fire({
                                text: 'Payment success',
                                icon: 'success',
                                confirmButtonText: 'OK'
                            });
                            break;
                        case 'fail':
                            Swal.fire({
                                text: 'Payment error',
                                icon: 'error',
                                confirmButtonText: 'OK'
                            });
                            break;
                        case 'process':
                            Swal.fire({
                                text: 'Please check your e-mail to verify payment and order status.\n' +
                                    'If you dont receive e-mail  - your transaction has been failed.\n',
                                icon: 'info',
                                confirmButtonText: 'OK'
                            });
                            break;
                        default:
                            break;
                    }
                });
            </script>
        @endif
    </div>

    <footer class="footer">
        <div class="container">
            <div class="footer-cols">
                <div class="col__left col__left--dn creature--footer">
                    <ul class="contacts-list">
                        <li class="contacts__item contacts__item--first">
                            <a class="contacts__link" href="tel:+380636629669">+380 63 662 9669</a>
                            <a class="contacts__link" href="tel:+380952301677">+380 95 230 1677</a>
                        </li>
                        <li class="contacts__item contacts__item--second"><a class="contacts__link" href="mailto:ua-dnepr@joymail.biz">ua-dnepr@joymail.biz</a> <a class="contacts__link" href="mailto:support@joymail.biz">support@joymail.biz</a></li>
                        <li class="contacts__item contacts__item--third"><span class="contacts__link">Dnipro, Kirgizskaya str., 3/29. SP Fedorenko N.O.</span></li>
                    </ul>
                    <ul class="socials-list">
                        <li class="socials__item"><a class="socials__link" target="_blank" href="https://www.facebook.com/groups/2389460657958736"><img src="{{asset('/img/icons/facebook.svg')}}" width="36" height="36" alt="Фейсбук" aria-label="Фейсбук"></a></li>
                        <li class="socials__item"><a class="socials__link" target="_blank" href="https://www.instagram.com/joymail.biz/"><img src="{{asset('/img/icons/instagram.svg')}}" width="36" height="36" alt="Инстаграм" aria-label="Инстаграм"></a></li>
                        <li class="socials__item"><a class="socials__link" target="_blank" href="https://www.youtube.com/channel/UCvzXZmwFkhq6JT3c0acS4ZA?view_as=subscriber"><img src="{{asset('/img/icons/youtube.svg')}}" width="36" height="36" alt="Ютуб" aria-label="Ютуб"></a></li>
                    </ul>
                </div>


                <div class="col__right">
                    <a class="logo--form" href="/{{$locale}}">
                        <img src="{{asset('/img/logo.svg')}}" width="165" height="65" alt="Логотип джойка">
                    </a>
                </div>
            </div>

            <div class="col__center">
                <p class="col__right-slogan">
                    <span class="orange">@lang('footer.slogan1')</span> - <span class="blue">@lang('footer.slogan2')</span>
                </p>
            </div>

            <div class="payment-labels">
                        <img src="/img/page2/liqpay.jpg" title="LiqPay invoice by E-mail">
                        <img src="/img/page2/visa.png" title="Visa">
                        <img src="/img/page2/master-card.png" title="Mastercard">
            </div>

            <br>
            <a class="col__center" style="color:#FFFFFF; text-decoration: underline;" href="/{{$locale}}/agreement">@lang('main.link_agree')</a>
            <div class="col__center" style="color:#FFFFFF; text-decoration: none;">
                ФОП Федоренко Надiя Олександрiвна. Р/р UA593052990000026000050274339
            </div>
        </div>
    </footer>
</header>
@endsection
